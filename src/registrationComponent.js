import React from "react";
import Form from "./applicationComponent/Form";
import { Field, reduxForm } from "redux-form";
import InputText from "./applicationComponent/InputText";
import RadioButton from "./applicationComponent/RadioButton";
import SelectBox from "./applicationComponent/SelectBox";
import CheckBox from "./applicationComponent/CheckBox";
import FileType from "./applicationComponent/FileType";
import Submit from "./applicationComponent/SubmitButton";
import HeaderTags from "./applicationComponent/HeaderTags";
import { Required, Email } from "./applicationComponent/ErrorMessage";
import {
  submitRegistration,
  getRegistration,
  getPreviewRegistration,
} from "./redux/actions/registration.action";
import { connect } from "react-redux";

export class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    // this.onBlurChange = this.onBlurChange.bind(this);
    this.validateField = this.validateField.bind(this);
  }

  returnUpdatedComponent = (componentType) => {
    if (componentType === "RadioButton") {
      return RadioButton;
    } else if (componentType === "SelectBox") {
      return SelectBox;
    } else if (componentType === "InputFile") {
      return FileType;
    } else if (componentType === "headerTags") {
      return HeaderTags;
    } else if (componentType === "CheckBox") {
      return CheckBox;
    } else {
      return InputText;
    }
  };

  validateField = (validateObj) => {
    let validFinal = [];
    if (validateObj.is_required === true) {
      validFinal.push(Required);
    }
    if (validateObj.valid_mail === true) {
      validFinal.push(Email);
    }

    return validFinal;
  };

  onChange(event) {
    //console.log("onchange Trigger");
    this.props.onChange({ showError: false });
    // return this.props.error === undefined
    //   ? this.props.completedTab(value)
    //   : null;
  }
  // onBlurChange() {
  //   this.setState({ isTouched: true });
  // }
  getPristine = (values) => {
    console.log(values, 60);
  };

  render() {
    let { formFields, onClick, error, onChange } = this.props;
    //console.log(error);

    return (
      <>
        {formFields.map((inputField, ind) => {
          return (
            <div key={ind} className="row">
              {inputField.sectionTabs.map((formDet, ind) => {
                return (
                  <div key={ind} className={`col-${formDet.colGrid}`} id={ind}>
                    <Form id="test" name="RegistrationForm">
                      <h3 className="title">{`${ind + 1}. ${
                        formDet.formname
                      }`}</h3>
                      <hr className="borderLine" />
                      <div className="row">
                        {formDet.formFields.map((inputData, pos) => {
                          return (
                            <div
                              key={pos}
                              className={`col-${inputData.colGrid} fieldStyle`}
                            >
                              <Field
                                component={this.returnUpdatedComponent(
                                  inputData.component_type
                                )}
                                name={inputData.field_nm}
                                label={`${inputData.field_label} ${
                                  inputData.field_validation.is_required
                                    ? "*"
                                    : ""
                                }`}
                                type={inputData.field_data_type}
                                icon={inputData.field_icon}
                                options={inputData.field_Options}
                                optionIcon={inputData.field_OptionsIcon}
                                pristine={(value) => this.getPristine(value)}
                                onChange={this.onChange}
                                // onBlur={(value) => this.onBlurChange(value)}
                                validate={this.validateField(
                                  inputData.field_validation
                                )}
                              />
                            </div>
                          );
                        })}
                      </div>
                    </Form>
                  </div>
                );
              })}
            </div>
          );
        })}
        <div className="col-12 nextServiceBtn">
          {this.props.serviceIcon.map((serviceIcon, pos) => {
            return (
              <Submit
                key={pos}
                serviceType={serviceIcon.serviceType}
                icon={serviceIcon.serviceIcon}
                onClick={onClick}
              />
            );
          })}
          {this.props.isServiceActive && !error && (
            <Submit
              serviceType={"next"}
              icon={""}
              name={"Next"}
              onClick={onClick}
            />
          )}
        </div>
      </>
    );
  }
}

// const mapStateToProps = (state) => {
//   let error =
//     state.form &&
//     state.form.RegistrationForm &&
//     state.form.RegistrationForm.syncErrors;
//   console.log(error);
//   return {
//     initialValues: state.register,
//     error: error,
//   };
// };
// const mapDispatchToProps = {
//   submitRegistration: () => submitRegistration(),
//   getRegistration: () => getRegistration(),
//   getPreviewRegistration: () => getPreviewRegistration(),
// };

// export default reduxForm({
//   form: "RegistrationForm",
//   enableReinitialize: true,
// })(connect(mapStateToProps, mapDispatchToProps)(Registration));

// // export default connect(mapStateToProps, mapDispatchToProps)(Registration);

export default Registration;
