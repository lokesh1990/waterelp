import React from "react";
import { Icons } from "./Icons/Icons.js";
import validate from "./ErrorMessage";

// export default class InputText extends React.Component {
//   constructor(props) {
//     super();
//     this.state = {
//       isTouched: false,
//     };
//     this.onChange = this.onChange.bind(this);
//     this.onBlurChange = this.onBlurChange.bind(this);
//   }
//   onChange(value) {
//     this.setState({ isTouched: true });
//     this.props.input.onChange(value);
//   }
//   onBlurChange() {
//     console.log(this.props);
//     debugger;
//     this.setState({ isTouched: true });
//   }

//   render() {
//     let {
//       input,
//       label,
//       type,
//       icon,
//       meta: { touched, error, warning },
//     } = this.props;
//     // console.log(touched);
//     return (
//       <div className="input-group">
//         <span
//           className={`input-group-addon iconclr ${
//             this.state.isTouched && error ? "required" : ""
//           }`}
//         >
//           <i className="glyphicon">{Icons(icon)}</i>
//         </span>
//         <input
//           id={input.name}
//           type={type}
//           className="form-control input"
//           name={input.name}
//           placeholder={label}
//           value={input.value}
//           onChange={this.onChange}
//           onBlur={this.onBlurChange}
//         />
//         <div>
//           {touched &&
//             ((error && <span>{error}</span>) ||
//               (warning && <span>{warning}</span>))}
//         </div>
//       </div>
//     );
//   }
// }

const InputText = ({
  input,
  label,
  type,
  icon,
  meta: { touched, error, warning, pristine },
}) => (
  <div className="input-group">
    <span
      className={`input-group-addon iconclr ${
        touched && error ? "required" : ""
      }`}
    >
      <i className="glyphicon">{Icons(icon)}</i>
    </span>
    <input
      id={input.name}
      type={type}
      className="form-control input"
      name={input.name}
      placeholder={label}
      pristine={pristine.toString()}
      {...input}
    />

    {/* {touched &&
      ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))} */}
  </div>
);

export default InputText;
