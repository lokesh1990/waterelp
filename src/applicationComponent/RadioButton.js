import React from "react";
import { Icons } from "./Icons/Icons.js";

export default class RadioButton extends React.Component {
  constructor(props) {
    super();
    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    this.setState({
      [event.target.id]: "active",
    });
    this.props.input.onChange(event.target.id);
  }

  render() {
    let {
      input,
      label,
      type,
      icon,
      options,
      optionIcon,
      meta: { touched, error, warning },
    } = this.props;

    return (
      <div className="input-group">
        <span className="input-group-addon iconclr">
          <i className="glyphicon">{Icons(icon)}</i>
        </span>
        <div className="checkGroup">
          <div className="btn-group btn-group-toggle" data-toggle="buttons">
            {options.map((optionsValue, pos) => {
              return (
                <label
                  key={pos}
                  htmlFor={optionsValue.toString().toLowerCase()}
                  className={`btn btn-secondary`}
                >
                  <input
                    type={type}
                    name={input.name}
                    id={optionsValue.toString().toLowerCase()}
                    autoComplete="off"
                    className="form-control input"
                    onChange={this.onChange}
                  />
                  <i className="glyphicon">{Icons(optionIcon[pos])}</i>
                </label>
              );
            })}
          </div>
        </div>

        <div>
          {touched &&
            ((error && <span>{error}</span>) ||
              (warning && <span>{warning}</span>))}
        </div>
      </div>
    );
  }
}
