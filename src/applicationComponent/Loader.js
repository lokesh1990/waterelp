import { Spinner } from 'spin.js';
import 'spin.js/spin.css';


var opts = {
    lines: 9, // The number of lines to draw
    length: 10, // The length of each line
    width: 52, // The line thickness
    radius: 37, // The radius of the inner circleq
    scale: 1, // Scales overall size of the spinner
    corners: 1, // Corner roundness (0..1)
    speed: 0.8, // Rounds per second
    rotate: 0, // The rotation offset
    animation: 'spinner-line-fade-more', // The CSS animation name for the lines
    direction: -1, // 1: clockwise, -1: counterclockwise
    color: '#3e0f0f', // CSS color or array of colors
    fadeColor: 'transparent', // CSS color or array of colors
    top: '45%', // Top position relative to parent
    left: '45%', // Left position relative to parent
    shadow: '0 0 1px transparent', // Box-shadow for the lines
    zIndex: 200000, // The z-index (defaults to 2e9)
    className: 'spinner', // The CSS class to assign to the spinner
    position: 'absolute', // Element positioning
};



var spinner = new Spinner(opts);

export const loaderOn = () => {
    spinner.spin(document.getElementById('app'));
}

export const loaderOff = () => {
    spinner.stop();
}

