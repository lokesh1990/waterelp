import React from "react";
import { Icons } from "./Icons/Icons.js";
import axios from "axios";

export default class FileType extends React.Component {
  constructor(props) {
    super();
    this.state = {
      // Initially, no file is selected
      selectedFile: null,
    };
    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    this.setState({
      [event.target.id]: "active",
    });
    this.props.input.onChange(event.target.id);
  }

  // On file upload (click the upload button)
  onFileUpload = (event, result) => {
    this.setState({ selectedFile: event.target.files[0] }, () => {
      // Create an object of formData
      const formData = new FormData();

      // Update the formData object
      formData.append(
        "myFile",
        this.state.selectedFile,
        this.state.selectedFile.name
      );
      this.props.input.onChange(this.state.selectedFile.name);

      // // Details of the uploaded file
      console.log(this.state.selectedFile);

      // // Request made to the backend api
      // // Send formData object
      // axios.post("http://localhost:3000/api", formData);
    });
  };

  render() {
    let {
      input,
      label,
      type,
      icon,
      options,
      meta: { touched, error, warning },
    } = this.props;
    // console.log(this.props.options.toString().toLowerCase());
    return (
      <div className="input-group">
        <span className="input-group-addon iconclr">
          <i className="glyphicon">{Icons(icon)}</i>
        </span>
        <div className="checkGroup">
          <div className="custom-file">
            <input
              type="file"
              className="custom-file-input input"
              id={input.name}
              placeholder={label}
              onChange={this.onFileUpload}
            />
            <label className="custom-file-label" htmlFor={input.name}>
              {label}
            </label>
          </div>
        </div>

        <div>
          {touched &&
            ((error && <span>{error}</span>) ||
              (warning && <span>{warning}</span>))}
        </div>
      </div>
    );
  }
}
