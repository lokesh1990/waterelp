import React from "react";
import { connect } from "react-redux";
import { Icons } from "./Icons/Icons.js";
import {
  getFormValues,
  getFormInitialValues,
  getFormSyncErrors,
  getFormMeta,
  getFormAsyncErrors,
  getFormSyncWarnings,
  getFormSubmitErrors,
  getFormError,
  getFormNames,
  isDirty,
  isPristine,
  isValid,
  isInvalid,
  isSubmitting,
  hasSubmitSucceeded,
  hasSubmitFailed,
} from "redux-form";
import { FormName } from "redux-form";

class Submit extends React.Component {
  render() {
    let { name, onClick, formName, state, icon, serviceType } = this.props;
    //console.log(serviceType);

    //console.log("------>", getFormSyncErrors(formName)(state));

    return (
      <button
        onClick={onClick}
        id={serviceType}
        type="button"
        className="btn nextBtn"
      >
        {icon && (
          <i style={{ fontSize: `12` }} className="material-icons">
            {Icons(icon)}
          </i>
        )}

        {name}
      </button>
    );
  }
}
const mapStateToProps = (state) => ({ state });

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Submit);
