export const AlertBar = (props) => {
  let msg = props.erroMsg;

  return (
    <div className="alert alert-danger alert-dismissible">
      <strong>{msg}</strong>
    </div>
  );
};
