import React from "react";
import { Icons } from "./Icons/Icons.js";

export default class SelectBox extends React.Component {
  constructor(props) {
    super();
    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {
    this.props.input.onChange(value);
    // console.log(this.props.input);
  }

  render() {
    let {
      input,
      label,
      type,
      icon,
      options,
      meta: { touched, error, warning },
    } = this.props;
    // console.log({ ...input }, 24);
    return (
      <div className="input-group">
        <span className="input-group-addon iconclr">
          <i className="glyphicon">{Icons(icon)}</i>
        </span>
        <div className="checkGroup">
          <select
            className="custom-select"
            id={input.name}
            onChange={this.onChange}
          >
            <option defaultValue>{input.value ? input.value : label}</option>
            {options.map((optionsValue, pos) => {
              return (
                <option key={pos} value={optionsValue}>
                  {optionsValue}
                </option>
              );
            })}
          </select>
        </div>
        <div>
          {touched &&
            ((error && <span>{error}</span>) ||
              (warning && <span>{warning}</span>))}
        </div>
      </div>
    );
  }
}
