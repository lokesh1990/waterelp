import React from "react";
import "./App.css";
import { Field, reduxForm } from "redux-form";
import InputText from "./applicationComponent/InputText";
import Submit from "./applicationComponent/SubmitButton";
import { Required } from "./applicationComponent/ErrorMessage";
// import { InterStialHandler } from "./applicationComponent/InterstialHandler";
import { Icons } from "./applicationComponent/Icons/Icons.js";
import { Registration } from "./registrationComponent";
import Formpreview from "./FormPreview";
import { connect } from "react-redux";
import { AlertBar } from "./applicationComponent/AlertBar";

import {
  submitRegistration,
  getRegistration,
  getPreviewRegistration,
  getServices,
} from "./redux/actions/registration.action";

// const getTypeComponent = (comType) => {};

export class App extends React.Component {
  constructor(props) {
    super();
    this.state = {
      serviceType: [],
      serviceIcon: [],

      isDetailsActive: true,
      isServiceActive: false,
      isPreviewActive: false,
      fieldValues: {},
      formFields: [],
      error: {},
      errorMsg: "",
      showError: false,
      isCompleted: false,
      formPreview: false,
      fieldsInput: [],
      serviceTypeFields: [],
    };
    this.getCheckedvalue = this.getCheckedvalue.bind(this);
    this.getServiceTab = this.getServiceTab.bind(this);
    this.serviceChange = this.serviceChange.bind(this);
    this.getPreviewFunction = this.getPreviewFunction.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  fieldsFilter = (filterBy) => {
    const filterFormFields = this.state.fieldsInput.filter(
      (items) => items.type === filterBy
    );
    return filterFormFields;
  };

  componentDidMount() {
    this.props
      .getRegistration()
      .then((res) => {
        this.setState({ fieldsInput: res.payLoad });
      })
      .then(() => {
        this.setState({ formFields: this.fieldsFilter("persoanlDetails") });
      })
      .catch((err) => {
        this.setState({ showError: true, errorMsg: "Internal Server Error" });
      });

    this.props
      .getServices()
      .then((res) => {
        this.setState({ serviceTypeFields: res.payLoad });
      })
      .catch((err) => {
        this.setState({ showError: true, errorMsg: "Internal Server Error" });
      });

    // this.setState({
    //   error: this.props.error,
    //   formFields: this.fieldsFilter("persoanlDetails"),
    // });

    // this.fieldsFilter("persoanlDetails");
  }
  componentWillReceiveProps(nextProps) {
    //return !nextProps.error ? console.log(nextProps, "------ new props") : "";
  }
  // componentWillUpdate(nextProps) {
  //   const { submitFailed, touch } = this.props;
  //   console.log(this.props.error, 108);

  //   if (submitFailed !== nextProps.submitFailed) {
  //     const toTouch = [];

  //     for (const key in this.props.error) {
  //       this.props.error.hasOwnProperty(key) && toTouch.push(key);
  //     }
  //     touch(...toTouch);
  //   }
  // }
  getCheckedvalue = (event) => {
    const isChecked = event.target.checked;
    if (isChecked) {
      this.setState({
        serviceType: [...this.state.serviceType, event.target.id],
        [event.target.name]: "checked",
        serviceIcon: [
          ...this.state.serviceIcon,
          {
            serviceIcon: event.target.getAttribute("icon"),
            serviceType: event.target.id,
          },
        ],
      });
    } else {
      this.state.serviceType.splice(
        this.state.serviceType.indexOf(event.target.id),
        1
      );

      this.state.serviceIcon.splice(
        this.state.serviceIcon.indexOf(event.target.getAttribute("icon")),
        1
      );

      this.setState({
        [event.target.name]: "",
      });
    }
  };
  serviceChange = (event) => {
    const type = event.target.id;

    if (type === "persoanlDetails") {
      this.setState({
        isDetailsActive: true,
        isServiceActive: false,
        isPreviewActive: false,
        formFields: this.fieldsFilter("persoanlDetails"),
      });
    }
    if (type === "service") {
      this.setState({
        isDetailsActive: false,
        isServiceActive: true,
        isPreviewActive: false,
        formFields: this.fieldsFilter(this.state.serviceType[0]),
      });
    }
    if (type === "preview") {
      this.setState({
        isDetailsActive: false,
        isServiceActive: false,
        isPreviewActive: true,
      });
    }
  };
  getServiceTab = (event) => {
    const { submitFailed, touch } = this.props;
    const typeId = event.target.id;

    let error = this.props.error;

    if (error) {
      const toTouch = [];
      for (const key in error) {
        error.hasOwnProperty(key) && toTouch.push(key);
      }
      touch(...toTouch);
      let getError = toTouch[0];
      this.setState({
        errorMsg: `${getError} ${error[getError]}`,
        showError: true,
      });
    }

    return typeId === "next" && !error
      ? this.props.getPreviewRegistration().then((values) => {
          this.setState({
            isDetailsActive: false,
            isPreviewActive: true,
            isServiceActive: false,
            fieldValues: values,
            showError: false,
            formPreview: true,
          });
        })
      : !error &&
          this.setState({
            formFields: this.fieldsFilter(event.currentTarget.id),
            isDetailsActive: false,
            isPreviewActive: false,
            isServiceActive: true,
            showError: false,
          });
  };
  getcompletedTab = (data) => {
    console.log(data, "-------Completed Tab");
  };
  async onSubmit(values) {
    this.setState({ isCompleted: true, formPreview: false });
    return this.props.submitRegistration();
  }
  formFieldOnChange = (values) => {
    this.setState(values);
  };

  async getPreviewFunction(event) {
    this.props.getPreviewRegistration().then((values) => {
      this.setState({
        isDetailsActive: false,
        isPreviewActive: true,
        isServiceActive: false,
        fieldValues: values,
      });
    });
    //await InterStialHandler(() => {
    //return this.props.submitRegistration();
    //});
  }
  render() {
    let { handleSubmit, error } = this.props;

    return (
      <div className="App">
        <div className="container regBackground">
          <div className="col-12">
            <div className="row btnRow">
              <div className="col-4">
                <button
                  type="button"
                  onClick={this.serviceChange}
                  id="persoanlDetails"
                  className={`btn left ${
                    this.state.isDetailsActive ? "active" : ""
                  }`}
                >
                  Your Details
                </button>
              </div>
              <div className="col-4">
                <button
                  type="button"
                  onClick={this.serviceChange}
                  id="service"
                  className={`btn center ${
                    this.state.isServiceActive ? "active" : ""
                  }`}
                >
                  Service
                </button>
              </div>
              <div className="col-4">
                <button
                  type="button"
                  onClick={this.serviceChange}
                  id="preview"
                  className={`btn right ${
                    this.state.isPreviewActive ? "active" : ""
                  }`}
                >
                  See Preview & Complete
                </button>
              </div>
            </div>
          </div>
          {/* Type of user displays below */}
          {!this.state.isPreviewActive && (
            <div className="col-12">
              <h3 className="title">1. Type of User</h3>
              <hr className="borderLine" />
            </div>
          )}
          {!this.state.isPreviewActive && (
            <div className="row userTypeBg">
              <div className="col-12">
                <div className="row">
                  {this.state.serviceTypeFields.map((item, pos) => {
                    return (
                      <div className="serviceImgGrid" key={pos}>
                        <span className="typeGrid">
                          <input
                            type="checkbox"
                            className="btn-check typeCheck"
                            id={item.serviceTypeId}
                            autoComplete="off"
                            onChange={this.getCheckedvalue}
                            value={item.serviceType}
                            name={item.serviceTypeId}
                            icon={item.serviceIcon}
                          />
                          <label
                            className={`btn btn-primary ${
                              this.state[item.serviceTypeId]
                            }`}
                            htmlFor={item.serviceTypeId}
                            disabled={this.state.isDetailsActive ? false : true}
                          >
                            <i
                              style={{ fontSize: `${item.fontSize}px` }}
                              className="material-icons"
                            >
                              {Icons(item.serviceIcon)}
                            </i>
                          </label>
                          <p>{item.serviceType}</p>
                        </span>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          )}
          {/* Registration component display below */}
          {!this.state.isPreviewActive && (
            <Registration
              onClick={this.getServiceTab}
              {...this.state}
              error={error}
              onChange={this.formFieldOnChange}
              completedTab={(value) => this.getcompletedTab(value)}
            />
          )}
          {this.state.showError && <AlertBar erroMsg={this.state.errorMsg} />};
          {this.state.isPreviewActive && (
            <Formpreview
              {...this.state}
              handleClick={this.serviceChange}
              serviceClick={this.getServiceTab}
            />
          )}
          <div className="col-12 nextServiceBtn">
            {this.state.isPreviewActive && !this.state.isCompleted && (
              <button
                onClick={handleSubmit(this.onSubmit)}
                id="next"
                type="button"
                className="btn nextBtn"
                formName={"RegistrationForm"}
              >
                Create Profile
              </button>
            )}
          </div>
          {/* Button trigger below */}
          {/* <div className="col-12 nextServiceBtn">
            {!this.state.isPreviewActive &&
              this.state.serviceIcon.map((serviceIcon, pos) => {
                return (
                  <button
                    key={pos}
                    onClick={this.getServiceTab}
                    id={serviceIcon.serviceType}
                    type="button"
                    className="btn nextBtn"
                  >
                    <i style={{ fontSize: `12` }} className="material-icons">
                      {Icons(serviceIcon.serviceIcon)}
                    </i>
                  </button>
                );
              })}
            {this.state.isServiceActive && (
              <button
                onClick={this.getPreviewFunction}
                id="next"
                type="button"
                className="btn nextBtn"
                formName={"RegistrationForm"}
              >
                Next
              </button>
            )}
          </div> */}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  let error =
    state.form &&
    state.form.RegistrationForm &&
    state.form.RegistrationForm.syncErrors;
  let completedFormFields = state.form.RegistrationForm;
  //console.log("error", error);
  return {
    initialValues: state.register,
    error: error,
    completedFormFields: completedFormFields,
  };
};
const mapDispatchToProps = {
  submitRegistration: () => submitRegistration(),
  getRegistration: () => getRegistration(),
  getPreviewRegistration: () => getPreviewRegistration(),
  getServices: () => getServices(),
};

export default reduxForm({
  form: "RegistrationForm",
  enableReinitialize: true,
})(connect(mapStateToProps, mapDispatchToProps)(App));
