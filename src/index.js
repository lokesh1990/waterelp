import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { Provider } from "react-redux";
import reportWebVitals from "./reportWebVitals";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import { store } from "./redux/configure-store.js";
import Formpreview from "./FormPreview";
import ExcelReader from "./excelReader/Excel.js";

const Preview = () => {
  return <div>Preview</div>;
};

// const App = () => {
//   return <div>App</div>;
// };

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path="/" component={App} />
        <Route path="/preview" component={Formpreview} />
        <Route path="/excelReader" component={ExcelReader} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
