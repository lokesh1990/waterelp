import { serviceCallThunk } from "../service-call-thunk";
import { fieldsInput } from "../../Apidata";
export const actionGetRegistration = "action/Get_Registration";

export const getRegistrationSuccess = (payLoad) => {
  return {
    type: actionGetRegistration,
    payLoad,
  };
};

export const getPreviewRegistration = () => {
  return async (dispatch, getState) => {
    try {
      const state = getState();
      const fieldValues = fieldsInput;
      const formData = state.form.RegistrationForm.values;
      Object.keys(formData).map((getkeys) => {
        const fieldKeys = getkeys;
        fieldValues.map((data) => {
          data.sectionTabs.map((innerFieldValue) => {
            innerFieldValue.formFields.map((innerFormFieldValue) => {
              return fieldKeys === innerFormFieldValue.field_nm
                ? (innerFormFieldValue.default_value = formData[fieldKeys])
                : "";
            });
          });
        });
      });

      console.log(fieldValues, 23);

      return formData;
    } catch (err) {
      throw err;
    }
  };
};

export const submitRegistration = () => {
  return async (dispatch, getState) => {
    try {
      const state = getState();
      const formData = state.form.RegistrationForm.values;
      const config = {
        url: `~^service, /v1/registration~^`,
        method: "POST",
        data: formData,
      };
      return await dispatch(serviceCallThunk(config));
    } catch (err) {
      throw err;
    }
  };
};

export const getRegistration = () => {
  return async (dispatch, getState) => {
    try {
      const state = getState();
      const config = {
        url: `http://localhost:3000/fieldsInput`,
        method: "GET",
      };
      return await dispatch(serviceCallThunk(config, getRegistrationSuccess));
    } catch (err) {
      throw err;
    }
  };
};

export const getServices = () => {
  return async (dispatch, getState) => {
    try {
      const state = getState();
      const config = {
        url: `http://localhost:3000/serviceTypeFields`,
        method: "GET",
      };
      return await dispatch(serviceCallThunk(config, getRegistrationSuccess));
    } catch (err) {
      throw err;
    }
  };
};
